import React from 'react';
import{
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Home from '../Tugas-2/Home'
import About from '../Tugas-2/About'
import Movies from '../Tugas-2/Movies'

const Headers = ()=>{
    return(
        <Router>
            <header>
                <nav>
                    <ul>
                        <li>
                        <Link to="/">Home</Link>
                          </li>
                        <li><Link to="/about">About</Link></li>
                        <li><Link to="/movies">Movie List</Link></li>
                        <li><Link to="/">Login</Link></li>
              
                    </ul>
                </nav>
            </header>

            <Switch>
            <Route exact path="/">
                <Home />
            </Route>
         
            <Route path="/about">
                <About />
              </Route>

              <Route path="/movies">
                <Movies />
              </Route>


            </Switch>
        </Router>
    )
}



export default Headers